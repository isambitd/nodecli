const chalk = require("chalk");
const clear = require("clear");
const figlet = require("figlet");
const CLI = require("clui");
const Spinner = CLI.Spinner;
const inquirer = require("./lib/inquirer");
const { getDetails } = require("./handler/getApiData");

var getSpinner = text =>
  new Spinner(text + " ...  ", ["⣾", "⣽", "⣻", "⢿", "⡿", "⣟", "⣯", "⣷"]);

clear();
console.log(
  chalk.yellow(figlet.textSync("LIQID", { horizontalLayout: "fitted" }))
);

const printData = details => {
  console.log("Output");
  details.output.forEach(el => {
    console.log(el);
  });
  console.log("\n\n");
  console.log("First 3 Drawdowns: ");
  const drawDownsToPrint =
    3 < details.drawDowns.length ? 3 : details.drawDowns.length;
  for (let i = 0; i < drawDownsToPrint; i++) {
    console.log(details.drawDowns[i]);
  }
  console.log("\n\n");
  console.log("Maximum drawdown: ");
  console.log(details.lowest);
  console.log("\n\n");
  console.log("Return: ");
  console.log(details.returnGrow);
};

const main = async () => {
  const waitingSpiiner = getSpinner("Processing");
  const inputs = await inquirer.getInputs();
  waitingSpiiner.start();
  const details = await getDetails(inputs);
  waitingSpiiner.message("\n");
  waitingSpiiner.stop();
  printData(details);
  if (details.err) {
    await main();
  } else {
    return;
  }
};

const start = async () => {
  await main();
  process.stdout.write("\n");
  process.exit();
};

start();
