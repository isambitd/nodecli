"use-strict";
const request = require("async-request");
const moment = require("moment");

const getApiUrl = inputs => {
  return `https://www.quandl.com/api/v3/datasets/WIKI/${inputs.stockSymbol.trim()}/data.json?api_key=${inputs.apiKey.trim()}&start_date=${
    inputs.startDate
  }&end_date=${inputs.endDate}`;
};

const validateDates = inputs => {
  const startDate = new Date(inputs.startDate);
  const endDate = new Date(inputs.endDate);

  if (isNaN(startDate.getTime()) || isNaN(endDate.getTime())) {
    return false;
  }
  return {
    ...inputs,
    startDate: moment.utc(startDate).format("YYYY-MM-DD"),
    endDate: moment.utc(endDate).format("YYYY-MM-DD")
  };
};

const getStructuredJSON = str => {
  const body = JSON.parse(str);
  const { data, column_names } = body.dataset_data;

  return data.map(eachData => {
    let newElement = {};
    eachData.forEach((element, index) => {
      newElement[column_names[index]] = element;
    });
    return newElement;
  });
};

const getOutput = data => {
  return data.map(
    el =>
      `${moment.utc(el.Date).format("DD.MM.YYYY")}: Closed at ${el.Close} (${
        el.Low
      } ~ ${el.High})`
  );
};

const getReturn = data => {
  if (data.length > 1) {
    const start = data.shift();
    const end = data.pop();
    const drawDown = end.Close - start.Close;
    const drawDownPercentage = parseInt((drawDown / start.Close) * 10000) / 100;
    return `${Math.abs(drawDown)}[${
      drawDown > 0 ? "+" : "-"
    }${drawDownPercentage}](${start.Close} on ${moment
      .utc(start.Date)
      .format("DD.MM.YYYY")} -> ${end.Close} on ${moment
      .utc(end.Date)
      .format("DD.MM.YYYY")})`;
  } else {
    ("0 [Start Date and End Date same]");
  }
};

const getDrawDowns = data => {
  let lowest = null;
  const drawDowns = data
    .map(el => {
      el.drawDown = el.Open > el.Close;
      el.drawDownVal = parseInt(((el.Open - el.Low) / el.Open) * 10000) / 100;
      el.drawDownText = `-${el.drawDownVal} (${el.High} on ${moment
        .utc(el.Date)
        .format("DD.MM.YYYY")} -> ${el.Low} on ${moment
        .utc(el.Date)
        .format("DD.MM.YYYY")})`;
      if (lowest) {
        lowest.drawDownVal < el.drawDownVal && (lowest = el);
      } else {
        lowest = el;
      }
      return el;
    })
    .filter(el => el.drawDown)
    .map(el => el.drawDownText);
  return {
    drawDowns,
    lowest: lowest.drawDownText
  };
};

const destructAndValidateRequest = async inputs => {
  inputs = validateDates(inputs);
  if (!inputs) {
    return { err: new Error("Invalid API Key!!!") };
  }
  const response = await request(getApiUrl(inputs));
  if (!response || !response.body) {
    return { err: new Error("Unable to fetch data!!!") };
  }

  const json = getStructuredJSON(response.body).reverse();
  const output = getOutput(json);

  const { lowest, drawDowns } = getDrawDowns(json);

  const returnGrow = getReturn(json);

  return { err: null, output, lowest, drawDowns, returnGrow };
};

exports.getDetails = destructAndValidateRequest;
