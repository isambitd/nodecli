"use-strict";
const inquirer = require("inquirer");

module.exports = {
  getInputs: () => {
    const questions = [
      {
        name: "apiKey",
        type: "input",
        message: "API_KEY: ",
        validate: function(value) {
          if (value) {
            return true;
          } else {
            return "API_KEY REQUIRED";
          }
        }
      },
      {
        name: "stockSymbol",
        type: "input",
        message: "Stock Symbol: ",
        validate: function(value) {
          if (value) {
            return true;
          } else {
            return "Stock Symbol REQUIRED ";
          }
        }
      },
      {
        name: "startDate",
        type: "input",
        message: "Start date: ",
        validate: function(value) {
          if (value) {
            return true;
          } else {
            return "Start date REQUIRED";
          }
        }
      },
      {
        name: "endDate",
        type: "input",
        message: "End date: ",
        validate: function(value) {
          if (value) {
            return true;
          } else {
            return "End date REQUIRED ";
          }
        }
      }
    ];
    return inquirer.prompt(questions);
  }
};
